//
//  GameScene.m
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import "GameScene.h"
#import "GameLayer.h"
#import "SWHUDLayer.h"


@implementation GameScene

-(id)init {
    if ((self = [super init])) {
        _gameLayer = [GameLayer node];
        [self addChild:_gameLayer z:0];
        _hudLayer = [SWHUDLayer node];
        [self addChild:_hudLayer z:1];
        
        _hudLayer.dpad.delegate = _gameLayer;
        _hudLayer.menuButton.delegate = _gameLayer;
        _gameLayer.hudLayer = _hudLayer;
    }
    return self;
}
@end

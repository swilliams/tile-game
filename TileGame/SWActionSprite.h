//
//  ActionSprite.h
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SWActionSprite : CCSprite {
    
}

// action
@property (nonatomic, strong) id idleAction;
@property (nonatomic, strong) id walkDownAction;
@property (nonatomic, strong) id walkUpAction;
@property (nonatomic, strong) id walkHorizontalAction;
@property (nonatomic, strong) id attackAction;

// states
@property (nonatomic, assign) ActionState actionState;

// attributes
@property (nonatomic, assign) float walkSpeed;

// movement
@property (nonatomic, assign) CGPoint velocity;
@property (nonatomic, assign) CGPoint desiredPosition;

// measurements
@property (nonatomic, assign) float centerToSides;
@property (nonatomic, assign) float centerToBottom;

// action emthods
- (void)idle;
- (void)attack;
- (void)walkWithDirection:(CGPoint)direction;

- (void)update:(ccTime)dt;
@end

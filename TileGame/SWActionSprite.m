//
//  ActionSprite.m
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import "SWActionSprite.h"


@implementation SWActionSprite

- (void)idle {
    if (self.actionState != kActionStateIdle) {
        [self stopAllActions];
//        [self runAction:self.idleAction];
        self.actionState = kActionStateIdle;
        self.velocity = CGPointZero;
    }
}


- (void)attack {
    if (self.actionState != kActionStateIdle || self.actionState ==kActionStateAttack || self.actionState == kActionStateWalk) {
        [self stopAllActions];
//        [self runAction:self.attackAction];
        self.actionState = kActionStateAttack;
    }
}

- (void)walkWithDirection:(CGPoint)direction {
    if (self.actionState == kActionStateIdle) {
        [self stopAllActions];
        if (direction.y > 0) {
            [self runAction:self.walkUpAction];
        } else if (direction.y < 0) {
            [self runAction:self.walkDownAction];
        } else {
            [self runAction:self.walkHorizontalAction];
        }
        self.actionState = kActionStateWalk;
    }
    if (self.actionState == kActionStateWalk) {
        self.velocity = ccp(direction.x * self.walkSpeed, direction.y * self.walkSpeed);
        self.scaleX = self.velocity.x >= 0 ? -1.0 : 1.0;
    }
}

- (void)update:(ccTime)dt {
    if (self.actionState == kActionStateWalk) {
        self.desiredPosition = ccpAdd(self.position, ccpMult(self.velocity, dt));
    }
}

@end

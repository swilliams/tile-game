//
//  GameLayer.h
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SWDpad.h"
#import "SWHudButton.h"

@class SWHero;
@class SWHUDLayer;

@interface GameLayer : CCLayer <SWDpadDelegate, SWHudButtonDelegate>  {
    CCTMXTiledMap *_tileMap;
    CCTMXLayer *_meta;
    CCTMXLayer *_foreground;
    CCSpriteBatchNode *_actors;
    SWHero *_hero;
}

@property (nonatomic, weak) SWHUDLayer *hudLayer;

@end

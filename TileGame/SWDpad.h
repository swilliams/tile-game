//
//  SWDpad.h
//  TileGame
//
//  Created by Scott Williams on 3/10/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class SWDpad;

@protocol SWDpadDelegate <NSObject>

- (void)dpad:(SWDpad *)dpad didChangeDirectionTo:(CGPoint)direction;
- (void)dpad:(SWDpad *)dpad isHoldingDirection:(CGPoint)direction;
- (void)dpadTouchEnded:(SWDpad *)dpad;

@end

@interface SWDpad : CCSprite <CCTouchOneByOneDelegate> {
    float _radius;
    CGPoint _direction;
}

@property (nonatomic, weak)id <SWDpadDelegate> delegate;
@property (nonatomic, assign)BOOL isHeld;

+ (id)dpadWithFile:(NSString *)fileName radius:(float)radius;
- (id)initWithFile:(NSString *)filename radius:(float)radius;

@end

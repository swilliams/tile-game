//
//  main.m
//  TileGame
//
//  Created by Scott Williams on 3/7/13.
//  Copyright Scott Williams 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
        return retVal;
    }
}

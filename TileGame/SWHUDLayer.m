//
//  SWHUDLayer.m
//  TileGame
//
//  Created by Scott Williams on 3/10/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "SWHUDLayer.h"
#import "SWDpad.h"
#import "SWHudButton.h"

@interface SWHUDLayer()

@end


@implementation SWHUDLayer

- (id) init {
    self = [super init];
    if (self) {
        [self showHUD];
    }
    return self;
}

- (void)showHUD {
    // nav hud
    _dpad = [SWDpad dpadWithFile:@"hud-cross.png" radius:100];
    _dpad.position = ccp(110, 110);
    _dpad.opacity = 0;
    
    [self addChild:_dpad];
    
    [_dpad runAction:[CCSequence actions:[CCDelayTime actionWithDuration:1.0f],
                    [CCFadeTo actionWithDuration:0.5f opacity:192],
                     nil]];
    
    _menuButton = [SWHudButton hudButtonWithFile:@"menu-button.png" title:@"Menu"];
    _menuButton.position = ccp(_menuButton.contentSize.width / 2 + 10, SCREEN.height - _menuButton.contentSize.height / 2 - 10);
    [self addChild:_menuButton];
}


@end

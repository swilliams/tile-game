//
//  SWChooseLayer.m
//  TileGame
//
//  Created by Scott Williams on 3/16/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import "SimpleAudioEngine.h"
#import "SWChooseLayer.h"
#import "SWGameState.h"
#import "GameScene.h"

@implementation SWChooseLayer

+ (CCScene *)scene {
    CCScene *scene = [CCScene node];
    
    SWChooseLayer *layer = [SWChooseLayer node];
    
    [scene addChild:layer];
    return scene;
}

- (id)init {
    if ((self = [super initWithColor:ccc4(10, 10, 10, 255)])) {
        // create rachel
        rachel = [CCSprite spriteWithFile:@"rachel-big.png"];
        rachel.position = ccp(SCREEN.width / 2 - rachel.contentSize.width - 100, SCREEN.height / 2);
        [self addChild:rachel];
        
        // create meredith
        meredith = [CCSprite spriteWithFile:@"meredith-big.png"];
        meredith.position = ccp(SCREEN.width / 2 + meredith.contentSize.width + 100, SCREEN.height / 2);
        [self addChild:meredith];
        
        
        self.touchEnabled = YES;
    }
    return self;
}

- (void)addPlayButton {
    if (!playButton) {
        playButton = [CCSprite spriteWithFile:@"play-big.png"];
        playButton.position = ccp(SCREEN.width / 2, playButton.contentSize.height + 50);
        [self addChild:playButton];
    }
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *t = [touches anyObject];
    CGPoint location = [self convertTouchToNodeSpace:t];
    if (CGRectContainsPoint(rachel.boundingBox, location)) {
        [[SWGameState sharedState] setHeroName:@"rachel"];
        NSString *soundEffect = [NSString stringWithFormat:@"%@-name.caf", [SWGameState sharedState].heroName];
        [[SimpleAudioEngine sharedEngine] playEffect:soundEffect];
    } else if (CGRectContainsPoint(meredith.boundingBox, location)) {
        [[SWGameState sharedState] setHeroName:@"meredith"];
        NSString *soundEffect = [NSString stringWithFormat:@"%@-name.caf", [SWGameState sharedState].heroName];
        [[SimpleAudioEngine sharedEngine] playEffect:soundEffect];
    }
    
    if ([SWGameState sharedState].heroName) {
        if (CGRectContainsPoint(playButton.boundingBox, location)) {
            NSString *goSound = [NSString stringWithFormat:@"%@-go.caf", [SWGameState sharedState].heroName];
            [[SimpleAudioEngine sharedEngine] playEffect:goSound];
            [self transitionToPlay];
        }
        [self addPlayButton];
    }
}

- (void)transitionToPlay {
   	[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[GameScene node] ]];
}

@end

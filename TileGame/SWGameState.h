//
//  SWGameState.h
//  TileGame
//
//  Created by Scott Williams on 3/16/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWGameState : NSObject

@property (nonatomic, copy) NSString *heroName;

+ (SWGameState *) sharedState;

@end

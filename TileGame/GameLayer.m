//
//  GameLayer.m
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import "SimpleAudioEngine.h"
#import "GameLayer.h"
#import "SWHero.h"
#import "SWGameState.h"
#import "SWChooseLayer.h"

@implementation GameLayer

-(id)init {
    if ((self = [super init])) {
        [self initTileMap];
    }
    return self;
}

-(void)initTileMap {
    _tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"house_tile.tmx"];
    for (CCTMXLayer *child in [_tileMap children]) {
        [[child texture] setAliasTexParameters];
    }
    [self addChild:_tileMap z:-6];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"%@-sprite.plist", [SWGameState sharedState].heroName]];
    _actors = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"%@-sprite.png", [SWGameState sharedState].heroName]];
    [_actors.texture setAliasTexParameters];
    [self addChild:_actors z:-5];
    
    [self scheduleUpdate];
    
    _meta = [_tileMap layerNamed:@"Meta"];
    _meta.visible = NO;
    NSAssert(_meta, @"No meta layer");
    
    CCTMXObjectGroup *objectGroup = [_tileMap objectGroupNamed:@"Objects"];
    NSAssert(objectGroup != nil, @"Tile map has no object layer");
    
    [self initCollectables];
    
    
    [self initHero:objectGroup];
}

#pragma mark - Audio
- (void)playSound:(NSString *)powerup {
    NSString *filename = [NSString stringWithFormat:@"%@-%@.caf", [[SWGameState sharedState] heroName], powerup];
    [[SimpleAudioEngine sharedEngine] playEffect:filename];
}

#pragma mark - Map Ops

- (void)initCollectables {
    _foreground = [_tileMap layerNamed:@"Foreground"];
    NSAssert(_foreground, @"No foregroud");
}

- (void)checkForCollectableAt:(CGPoint)coords properties:(NSDictionary *)properties{
    NSString *collectible = properties[@"Collectable"];
    if (collectible && [collectible isEqualToString:@"True"]) {
        int tileGid = [_foreground tileGIDAt:coords];
        NSDictionary *foregroundProperties = [_tileMap propertiesForGID:tileGid];
        NSString *powerup = foregroundProperties[@"powerup-name"];
        [_meta removeTileAt:coords];
        [_foreground removeTileAt:coords];
        CCLOG(@"GOT %@", powerup);
        [self playSound:powerup];
    }
}

- (void)initHero:(CCTMXObjectGroup *)objectGroup{
    NSDictionary *spawnPoint = [objectGroup objectNamed:@"SpawnPoint"];
    int x = [spawnPoint[@"x"] integerValue];
    int y = [spawnPoint[@"y"] integerValue];
    _hero = [SWHero node];
    
    
    [self addChild:_hero z:2];
    // [_actors addChild:_hero];
    
    _hero.position = ccp(x,y);
    _hero.desiredPosition = _hero.position;
    
    [self setViewPointCenter:_hero.position];
    
    [_hero idle];
    
}

- (void)setViewPointCenter:(CGPoint)position {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    // make sure the camera won't be below the bounds
    int x = MAX(position.x, winSize.width/2);
    int y = MAX(position.y, winSize.height/2);
    // make sure the camera won't be beyond the bounds
    x = MIN(x, (_tileMap.mapSize.width * _tileMap.tileSize.width) - winSize.width / 2);
    y = MIN(y, (_tileMap.mapSize.height * _tileMap.tileSize.height) - winSize.height/2);
    CGPoint actualPosition = ccp(x, y);
    
    CGPoint centerOfView = ccp(winSize.width/2, winSize.height/2);
    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);
    self.position = viewPoint;
}

# pragma mark - tick
- (void)update:(ccTime)delta {
    [_hero update:delta];
    [self updatePositions];
    [self setViewPointCenter:_hero.position];
}

- (void)updatePositions {
    float posX = MIN(_tileMap.mapSize.width * _tileMap.tileSize.width - _hero.centerToSides, MAX(_hero.centerToSides, _hero.desiredPosition.x));
    float posY = MIN(_tileMap.mapSize.height * _tileMap.tileSize.height + _hero.centerToBottom, MAX(_hero.centerToBottom, _hero.desiredPosition.y));
    CGPoint pos = ccp(posX, posY);
    if ([self canMoveTo:pos]) {
        _hero.position = pos;
    }
}

# pragma mark - boundaries
- (BOOL)canMoveTo:(CGPoint)position {
    CGPoint tileCoord = [self tileCoordForPosition:position];
    int tileGid = [_meta tileGIDAt:tileCoord];
    if (tileGid) {
        NSDictionary *props = [_tileMap propertiesForGID:tileGid];
        if (props) {
            NSString *collision = props[@"Collidable"];
            [self checkForCollectableAt:tileCoord properties:props];
            if (collision && [collision isEqualToString:@"True"]) {
                return NO;
            }
        }
    }
    return YES;
}


# pragma mark - DPAD
- (void)dpad:(SWDpad *)dpad didChangeDirectionTo:(CGPoint)direction {
    [_hero walkWithDirection:direction];
}

- (CGPoint)tileCoordForPosition:(CGPoint)position {
    int x = position.x / _tileMap.tileSize.width;
    int y = ((_tileMap.mapSize.height * _tileMap.tileSize.height) - position.y) / _tileMap.tileSize.height;
    return ccp(x, y);
}

- (void)dpadTouchEnded:(SWDpad *)dpad {
    if (_hero.actionState == kActionStateWalk) {
        [_hero idle];
    }
}

- (void)dpad:(SWDpad *)dpad isHoldingDirection:(CGPoint)direction {
    [_hero walkWithDirection:direction];
}

# pragma mark - hud buttons
- (void)hudButtonPressed:(SWHudButton *)button {
    if ([button.title isEqualToString:@"Menu"]) {
        [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[SWChooseLayer scene]]];
    }
}

# pragma mark - teardown
- (void)dealloc {
    [self unscheduleUpdate];
}


@end

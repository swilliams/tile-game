//
//  GameScene.h
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class GameLayer;
@class SWHUDLayer;

@interface GameScene : CCScene {
    
}

@property (nonatomic, weak) GameLayer *gameLayer;
@property (nonatomic, weak) SWHUDLayer *hudLayer;

@end

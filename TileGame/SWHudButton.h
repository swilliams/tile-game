//
//  SWHudButton.h
//  TileGame
//
//  Created by Scott Williams on 3/17/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class SWHudButton;

@protocol SWHudButtonDelegate <NSObject>

- (void)hudButtonPressed:(SWHudButton *)button;

@end

@interface SWHudButton : CCSprite <CCTouchOneByOneDelegate> {
    
}

@property (nonatomic, copy)NSString *title;
@property (nonatomic, weak)id <SWHudButtonDelegate> delegate;

+ (id)hudButtonWithFile:(NSString *)filename title:(NSString *)title;
- (id)initWithFile:(NSString *)filename title:(NSString *)title;

@end

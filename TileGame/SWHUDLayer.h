//
//  SWHUDLayer.h
//  TileGame
//
//  Created by Scott Williams on 3/10/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "cocos2d.h"
@class SWDpad;
@class SWHudButton;


@interface SWHUDLayer : CCLayer {
}

@property (nonatomic, weak)SWDpad *dpad;
@property (nonatomic, strong)SWHudButton *menuButton;

- (void)showHUD;

@end

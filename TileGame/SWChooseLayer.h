//
//  SWChooseLayer.h
//  TileGame
//
//  Created by Scott Williams on 3/16/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SWChooseLayer : CCLayerColor {
    CCSprite *rachel;
    CCSprite *meredith;
    CCSprite *playButton;
}

+ (CCScene *) scene;

@end

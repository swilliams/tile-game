//
//  SWHudButton.m
//  TileGame
//
//  Created by Scott Williams on 3/17/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import "SWHudButton.h"


@implementation SWHudButton

+ (id)hudButtonWithFile:(NSString *)filename title:(NSString *)title {
    return [[self alloc] initWithFile:filename title:title];
}

- (id)initWithFile:(NSString *)filename title:(NSString *)title {
    if ((self = [super initWithFile:filename])) {
        self.title = title;
    }
    return self;
}

- (void)onEnterTransitionDidFinish {
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:1 swallowsTouches:YES];
}

# pragma mark - Touch Logic
- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint location = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
    if (CGRectContainsPoint([self boundingBox], location)) {
        [_delegate hudButtonPressed:self];
        return YES;
    }
    return NO;
}


@end

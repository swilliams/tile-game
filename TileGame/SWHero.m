//
//  SWHero.m
//  TileGame
//
//  Created by Scott Williams on 3/15/13.
//  Copyright 2013 Scott Williams. All rights reserved.
//

#import "SWHero.h"
#import "SWGameState.h"

@implementation SWHero

- (id)init {
    if ((self = [super initWithSpriteFrameName:[NSString stringWithFormat:@"%@-sprite_01.png", [SWGameState sharedState].heroName]])) {
        [self initWalkActions];
        
        self.centerToBottom = 24;
        self.centerToSides = 16;
        self.walkSpeed = 160;
    }
    
    return self;
}

- (CCAnimation *)createWalkAnimationWithFrame:(int)frame {
    int numberOfFramesPerAnimation = 4;
    NSMutableArray *frames = [NSMutableArray arrayWithCapacity:numberOfFramesPerAnimation];
    for (int i = frame; i < frame + numberOfFramesPerAnimation; i++) {
        [frames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%@-sprite_%02d.png", [SWGameState sharedState].heroName, i]]];
    }
    
    CCAnimation *walkAnimation = [CCAnimation animationWithSpriteFrames:frames delay:1.0/12];
    return walkAnimation;
}

- (void)initWalkActions {
    CCAnimation *walkDownAnimation = [self createWalkAnimationWithFrame:1];
    self.walkDownAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkDownAnimation]];
    
    CCAnimation *walkUpAnimation = [self createWalkAnimationWithFrame:9];
    self.walkUpAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkUpAnimation]];
    
    CCAnimation *walkHorizontalAnimation = [self createWalkAnimationWithFrame:5];
    self.walkHorizontalAction = [CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:walkHorizontalAnimation]];
}


@end

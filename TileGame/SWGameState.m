//
//  SWGameState.m
//  TileGame
//
//  Created by Scott Williams on 3/16/13.
//  Copyright (c) 2013 Scott Williams. All rights reserved.
//

#import "SWGameState.h"

@implementation SWGameState

+ (SWGameState *)sharedState {
    static SWGameState *state = nil;
    if (!state) {
        state = [[super allocWithZone:nil] init];
    }
    return state;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedState];
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
